
export class consulta {
  error: boolean;
  msg: string;
  data: Vendedor[];
}

export class Vendedor {
  id: number;
  nombre: string;
  rut: string;
  clientes: Cliente[];
  contactados : number=0;
  contactadosFallidos : number =0;
  minLlamadas : number =0;
  subgerente: string;
  codTienda:number;
  tienda: string;
  totalVenta:number;
  ventas: any[];
  fechaInicio:any;
  fechaTermino:any
}

export class Cliente {
  _id: string;
  rut: string;
  nombre: string;
  celular: string;
  idvd: number;
  rankin: number;
  clvd: number;
  registros: Registro[];
  llamadasNoContestadas:Registro[];
  llamadasContestadas:Registro[];
}

export class Registro {
  _id: string;
  fecha: string;
  segundos : number;
  idllamada:string
}