import { Component } from '@angular/core';

import { NgbDatepickerConfig, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { Idioma } from './config/idioma';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  /* providers: [{provide: NgbDateParserFormatter, useClass: Idioma}] */
})
export class AppComponent {
  title = 'implementosLlamadaWeb';
}
