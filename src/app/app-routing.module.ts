import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/homePage/page/home/home.component';
import { LlamadasComponent } from './pages/llamadasPage/page/llamadas/llamadas.component';


const routes: Routes = [
  { path: 'home',  component: HomeComponent },
  { path: 'llamadas',  component: LlamadasComponent },
  { path: '',   redirectTo: '/', pathMatch: 'full' },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,  { useHash: true }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
