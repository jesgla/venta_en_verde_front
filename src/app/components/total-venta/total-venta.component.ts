import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'total-venta',
  templateUrl: './total-venta.component.html',
  styleUrls: ['./total-venta.component.scss']
})
export class TotalVentaComponent implements OnInit {
@Input() totalVentas: number;
  constructor() { }

  ngOnInit() {
  }

}
