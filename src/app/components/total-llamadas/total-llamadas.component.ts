import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'total-llamadas',
  templateUrl: './total-llamadas.component.html',
  styleUrls: ['./total-llamadas.component.scss']
})
export class TotalLlamadasComponent implements OnInit {
@Input() titulo : string;
@Input() totalLlamadas : number;
  constructor() { }

  ngOnInit() {
  }

}
