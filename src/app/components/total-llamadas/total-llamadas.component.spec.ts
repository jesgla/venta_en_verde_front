import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalLlamadasComponent } from './total-llamadas.component';

describe('TotalLlamadasComponent', () => {
  let component: TotalLlamadasComponent;
  let fixture: ComponentFixture<TotalLlamadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalLlamadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalLlamadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
