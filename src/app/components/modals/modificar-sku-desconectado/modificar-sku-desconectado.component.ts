import { Component, OnInit, Input, Output, EventEmitter   } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ArticulosService } from 'src/app/services/articulos/articulos.service';

@Component({
  selector: 'app-modificar-sku-desconectado',
  templateUrl: './modificar-sku-desconectado.component.html',
  styleUrls: ['./modificar-sku-desconectado.component.scss']
})
export class ModificarSkuDesconectadoComponent implements OnInit {
  @Input()modalEditaRef:BsModalRef;
  @Input()artEditar:any;
  @Output() public actualizar = new EventEmitter<any>();
  formEditar: FormGroup;
  constructor(
    private fb: FormBuilder,
    private articulosv: ArticulosService  
  ) {
    // this.formEditar = this.fb.group(
    //   {
    //     sku: new FormControl(this.artEditar.sku, { validators: [Validators.required] }),
    //     nparte: new FormControl(this.artEditar.nparte, { validators: [Validators.required] }),
    //     stock: new FormControl(this.artEditar.stock, { validators: [Validators.required] }),        
    //   }
    // )
    console.log('formulario', this.formEditar);
  }

  ngOnInit() {
    console.log("artEditar",this.artEditar);
    this.formEditar = this.fb.group(
      {
        sku: new FormControl(this.artEditar.sku, { validators: [Validators.required] }),
        nparte: new FormControl(this.artEditar.nparte, { validators: [Validators.required] }),
        stock: new FormControl(this.artEditar.stock, { validators: [Validators.required] }),        
      }
    )
  }

  async guardarDatos() {
    //let {nombre,edad,correo,direccion}=this.formCrear.value;
    let objeto = this.formEditar.value;
    let respuesta = await this.articulosv.CreaModificaSkuDesconectado(objeto);

    if (!respuesta.error){
      this.actualizar.emit(true);
      this.modalEditaRef.hide();
    }
    console.log('respuesta',respuesta)
  }

}
