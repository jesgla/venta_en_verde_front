import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarSkuDesconectadoComponent } from './modificar-sku-desconectado.component';

describe('ModificarSkuDesconectadoComponent', () => {
  let component: ModificarSkuDesconectadoComponent;
  let fixture: ComponentFixture<ModificarSkuDesconectadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarSkuDesconectadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarSkuDesconectadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
