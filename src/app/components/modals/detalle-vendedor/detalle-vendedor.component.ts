import { Component, OnInit, Input } from '@angular/core';
import { ModalService } from 'src/app/services/modal/modal.service';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';

@Component({
  selector: 'app-detalle-vendedor',
  templateUrl: './detalle-vendedor.component.html',
  styleUrls: ['./detalle-vendedor.component.scss']
})
export class DetalleVendedorComponent implements OnInit {
  @Input() close: any;
  vendedor: any;
  lstClientes: any;
  lstContestadas: any[];
  lstNoContestadas: any[];
  constructor(private dataModal: ModalService, private vendedorService: VendedorService) { 
    this.lstContestadas=[]
    this.lstNoContestadas=[]
  }


  async ngOnInit() {
    this.vendedor = this.dataModal.vendedor;

    this.lstClientes = this.vendedor.clientes.filter(cliente => cliente.registros.length > 0);
    for (let index = 0; index < this.lstClientes.length; index++) {
      let lstSi = null;
      let lstNo = null;
      const cliente = this.lstClientes[index];
      let comentarios= await this.vendedorService.obtenerEventos(cliente,this.vendedor);
      lstSi = cliente.registros.filter((llamada,index) => {
      
        llamada.rut = cliente.rut;
        llamada.nombre = cliente.nombre;
        llamada.celular = cliente.celular;
        //llamada.comentario = comentarios && comentarios.length>0 && comentarios[index].Observaciones?comentarios[index].Observaciones:'Sin comentario';
        if(comentarios.length>0 && comentarios[index]!=undefined){
          llamada.comentario =comentarios[index].Observaciones
        }else{
          llamada.comentario ='Sin comentario'
        }
        
        if (llamada.segundos > 0) {
          this.lstContestadas.push(llamada)
        }else if(llamada.segundos ==0){
          this.lstNoContestadas.push(llamada)
        }

      });

    }
   

  }
  cerrarModal() {
    this.close()
  }

  exportAsXLSX(): void {
    let lstExcel = [[this.vendedor.rut, this.vendedor.nombre]];
    let noContestadas = [];
    let contestadas = [];

    for (let index = 0; index < this.lstClientes.length; index++) {
      const cliente = this.lstClientes[index];
      for (let indice = 0; indice < cliente.registros.length; indice++) {
        const llamada = cliente.registros[indice];
        let objetoArr = [cliente.rut, cliente.nombre, cliente.celular, llamada.segundos == undefined ? 0 : llamada.segundos, llamada.idllamada == undefined ? llamada.fecha : new Date(Number(llamada.idllamada)) , llamada.comentario];
        /*   let objeto ={
            rut: cliente.rut,
            nombre: cliente.nombre,
            celular: cliente.celular,
            segundos : llamada.segundos ==undefined?0:llamada.segundos,
            fecha:llamada.idllamada ==undefined?llamada.fecha:new Date(Number(llamada.idllamada)), 
          } */
        if (llamada.segundos > 0) {
          contestadas.push(objetoArr)
        } else {
          noContestadas.push(objetoArr)
        }

      }

    }
    lstExcel.push([]);
    lstExcel.push(["Llamadas contestadas"]);
    lstExcel.push([]);
    lstExcel.push(['rut', 'nombre', 'celular', 'segundos', 'fecha','comentario']);

    contestadas.map(llamada => { lstExcel.push(llamada) })

    lstExcel.push([]);
    lstExcel.push(["Llamadas no contestadas"]);
    lstExcel.push([]);
    lstExcel.push(['rut', 'nombre', 'celular', 'segundos', 'fecha','comentario']);
    noContestadas.map(llamada => { lstExcel.push(llamada) })
    /*     lstExcel.sort((a, b) => {
          if (a.rut > b.rut) {
            return 1;
          }
          if (a.rut < b.rut) {
            return -1;
          }
          // a must be equal to b
          return 0;
        }); */

    this.vendedorService.exportAsExcelFile(lstExcel, 'vendedor');
  }
}
