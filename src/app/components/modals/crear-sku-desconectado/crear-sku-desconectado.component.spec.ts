import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearSkuDesconectadoComponent } from './crear-sku-desconectado.component';

describe('CrearSkuDesconectadoComponent', () => {
  let component: CrearSkuDesconectadoComponent;
  let fixture: ComponentFixture<CrearSkuDesconectadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearSkuDesconectadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearSkuDesconectadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
