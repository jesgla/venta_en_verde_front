import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ArticulosService } from 'src/app/services/articulos/articulos.service';

@Component({
  selector: 'app-crear-sku-desconectado',
  templateUrl: './crear-sku-desconectado.component.html',
  styleUrls: ['./crear-sku-desconectado.component.scss']
})
export class CrearSkuDesconectadoComponent implements OnInit {
  @Input()modalCreaRef:BsModalRef;
  @Output() public actualizar = new EventEmitter<any>();
  formCrear: FormGroup;
  constructor(
    private fb: FormBuilder,
    private articulosv: ArticulosService  
  ) {
    this.formCrear = this.fb.group(
      {
        sku: new FormControl(null, { validators: [Validators.required] }),
        nparte: new FormControl(null, { validators: [Validators.required] }),
        stock: new FormControl(null, { validators: [Validators.required] }),        
      }
    )
    console.log('formulario', this.formCrear);
  }

  ngOnInit() {
  }

  async guardarDatos() {
    //let {nombre,edad,correo,direccion}=this.formCrear.value;
    let objeto = this.formCrear.value;
    let respuesta = await this.articulosv.CreaModificaSkuDesconectado(objeto);

    if (!respuesta.error){
      this.actualizar.emit(true);
      this.modalCreaRef.hide();
    }
    console.log('respuesta',respuesta)
  }


}
