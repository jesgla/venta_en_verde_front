import { Component, OnInit, Input, Output, EventEmitter    } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as XLSX from 'xlsx';
import { ArticulosService } from 'src/app/services/articulos/articulos.service';

@Component({
  selector: 'app-masiva-sku-desconectado',
  templateUrl: './masiva-sku-desconectado.component.html',
  styleUrls: ['./masiva-sku-desconectado.component.scss']
})
export class MasivaSkuDesconectadoComponent implements OnInit {
  @Input()modalMasivaRef:BsModalRef;
  file: File;
  arrayBuffer: any;
  lstDesconxls:any[];  
  @Output() public actualizar = new EventEmitter<any>();
  constructor(    
    private articulosv: ArticulosService
  ) {    
    this.lstDesconxls=[];
   }

  ngOnInit() {
  }

  incomingfile(event) {
    this.file = event.target.files[0];
  }
  onFileChange() {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      let json = XLSX.utils.sheet_to_json(worksheet, { raw: true })
      this.lstDesconxls = json;
      console.log('json',json);
      //this.insertarProductos(json);

    }
    fileReader.readAsArrayBuffer(this.file);
  }
  
  async guardarDatos() {
    //let {nombre,edad,correo,direccion}=this.formCrear.value;
    let lstCargadosxls:any[];
    lstCargadosxls=[];
    for (let index = 0; index < this.lstDesconxls.length; index++) {
      const objeto = this.lstDesconxls[index];
      let respuesta = await this.articulosv.CreaModificaSkuDesconectado(objeto);
      if (!respuesta.error){
        lstCargadosxls.push(objeto)        
      }
    }
    if (lstCargadosxls.length>0){
      this.actualizar.emit(true);
      this.modalMasivaRef.hide();

    }
    // this.lstDesconxls.map(async objeto=>{
    //   let respuesta = await this.articulosv.CreaModificaSkuDesconectado(objeto);
    //   if (!respuesta.error){
    //     lstCargadosxls.push(objeto)        
    //   }
      
    //  })
    
  }


}
