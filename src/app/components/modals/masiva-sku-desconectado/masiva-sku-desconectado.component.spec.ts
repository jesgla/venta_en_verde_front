import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasivaSkuDesconectadoComponent } from './masiva-sku-desconectado.component';

describe('MasivaSkuDesconectadoComponent', () => {
  let component: MasivaSkuDesconectadoComponent;
  let fixture: ComponentFixture<MasivaSkuDesconectadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasivaSkuDesconectadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasivaSkuDesconectadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
