import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HomeComponent } from './pages/homePage/page/home/home.component';
import { VendedorService } from './services/vendedor/vendedor.service';

import {NgbModule, NgbDatepickerI18n, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import { DetalleVendedorComponent } from './components/modals/detalle-vendedor/detalle-vendedor.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { Ng2Rut } from 'ng2-rut';
import {NgxSpinnersModule} from 'ngx-spinners';

import { PipesModule } from './pipes/pipes.module';
import { LlamadasComponent } from './pages/llamadasPage/page/llamadas/llamadas.component';

import { LlamadaTableComponent } from './pages/llamadasPage/components/llamada-table/llamada-table.component';
import { VentasTableComponent } from './pages/llamadasPage/components/ventas-table/ventas-table.component';
import { TotalVentaComponent } from './components/total-venta/total-venta.component';
import { TotalLlamadasComponent } from './components/total-llamadas/total-llamadas.component';
import { Calendario, I18n } from './config/calendario';
import { Idioma } from './config/idioma';
import { CrearSkuDesconectadoComponent } from './components/modals/crear-sku-desconectado/crear-sku-desconectado.component';
import { ModificarSkuDesconectadoComponent } from './components/modals/modificar-sku-desconectado/modificar-sku-desconectado.component';
import { MasivaSkuDesconectadoComponent } from './components/modals/masiva-sku-desconectado/masiva-sku-desconectado.component';
import { ModalModule } from'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    DetalleVendedorComponent,
    LlamadasComponent,
    LlamadaTableComponent,
    VentasTableComponent,
    TotalVentaComponent,
    TotalLlamadasComponent,
    CrearSkuDesconectadoComponent,
    ModificarSkuDesconectadoComponent,
    MasivaSkuDesconectadoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    PipesModule,
    Ng2Rut,
    NgxSpinnersModule,
    ModalModule.forRoot()
  ],
  providers: [
    VendedorService,
    I18n,
    {provide: NgbDatepickerI18n, useClass: Calendario},
/*     {provide: NgbDateParserFormatter, useClass: Idioma} */
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
