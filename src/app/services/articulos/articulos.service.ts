import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArticulosService {

  /**
   *Creates an instance of ArticulosService.
   * @param {HttpClient} httpClient
   * @memberof ArticulosService
   */
  constructor(private httpClient: HttpClient) { }
  /**
   * header para ejecutar consultas post
   * @memberof ArticulosService
   */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin':'*',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'
    });

  /**
   *Permite obtener listado de articulos
   * @param {*} parametros
   * @returns
   * @memberof ArticulosService
   */
  async obtenerArticulos(parametros) {
    let consulta = null;

    const url = `https://b2b-api.implementos.cl/api/movil/busquedaProductos`;
    //const url =  `${environment.endPointB2B}${environment.Articulos.methods.obtenerArticulos}`;


    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
 
    return consulta;
  }
  async obtenerDesconectados(parametros) {
    let consulta = null;

    const url = `WebApiVerde/api/buscador/desconectados?sku=`;
    //const url =  `${environment.endPointB2B}${environment.Articulos.methods.obtenerArticulos}`;


    consulta = await this.httpClient.get(url, { headers: this.header }).toPromise();
 
    return consulta;
  }
 
  async CreaModificaSkuDesconectado(parametros) {
    let consulta = null;

    const url = `WebApiVerde/api/buscador/agregadesconectado?sku=${parametros.sku}&nparte=${parametros.nparte}&stock=${parametros.stock}&urlconectado=&desconectado=SI&usuario=0`;
    //const url = `http://replicacion.implementos.cl/WebApiVerde/api/buscador/agregadesconectado?sku=AGREDB0011&nparte=298239212212&stock=260&urlconectado=&desconectado=SI&usuario`;
    //const url =  `${environment.endPointB2B}${environment.Articulos.methods.obtenerArticulos}`;


    consulta = await this.httpClient.get(url, { headers: this.header }).toPromise();
 
    return consulta;
  }
}
