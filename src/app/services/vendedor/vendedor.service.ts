import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { consulta } from 'src/app/interfaces/vendedor';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class VendedorService {
  /**
     *header para peticiones post
     * @memberof EventoService
     */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json',
      'response-Type': 'json'
    }

  );
  constructor(private httpClient: HttpClient) { }

  async obtenerDatosInforme(inicio,termino) {
    let consulta = null
    let parametros={
      "inicio":inicio.toString(),
      "termino":termino.toString()
    }
    // var dateinicio = (inicio.getFullYear() + ("0"+(inicio.getMonth()+1)).slice(-2) + ("0" + inicio.getDate()).slice(-2));
    // var datetermino = (termino.getFullYear() + ("0"+(termino.getMonth()+1)).slice(-2) + ("0" + termino.getDate()).slice(-2));    
    var dateinicio = inicio;
    var datetermino = termino; 

    console.log('inicio:',dateinicio.toString());
    console.log('termino:',datetermino.toString());
    //const url = `http://localhost:1900/api/movil/clientesvendedor`;
    const url = `http://replicacion.implementos.cl/wsOmnichannel/Capa_jsonServices/getDatosDropShipment.ashx?intCodInforme=2&strParametro1=` + dateinicio + `&strParametro2=` + datetermino + `&strParametro3=&strParametro4=&strParametro5=`;
    
    consulta = await this.httpClient.get(url).toPromise();
    console.log('obtenerDatosInforme:',consulta);
  
    return consulta;
  }
  async obtenerArticulosDropShipment() {
    let consulta = null    
    //const url = `http://localhost:1900/api/movil/clientesvendedor`;
    const url = `http://replicacion.implementos.cl/wsOmnichannel/Capa_jsonServices/getDatoArticuloDrop.ashx`;
    
    consulta = await this.httpClient.get(url).toPromise();
    console.log('obtenerArticulosDropShipment:',consulta);
  
    return consulta;
  }
  public exportAsExcelFile(json: any[], excelFileName: string): void {

    const worksheet: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  async obtenerEventos(cliente, vendedor) {
    let consulta = null;

    let parametros = {
      strTipoEvento: 'COMENTARIO',
      rutVendedor: vendedor.rut,
      rutCliente: cliente.rut.replace('.', '').replace('.', ''),

    }
    //const url = `http://localhost:1900/api/movil/obtenerEventosCliente`;

    const url = `https://b2b-api.implementos.cl/api/movil/obtenerEventosCliente`;

    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
 
    return consulta;
  }
}
