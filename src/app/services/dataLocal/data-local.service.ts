import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  constructor() { }
  guardarItem(tipo, datos) {
    localStorage.removeItem(tipo);
    localStorage.setItem(tipo, JSON.stringify(datos));
  }

  async obtenerItem(tipo){
    let datos = await localStorage.getItem(tipo);
    return await JSON.parse(datos);
  }
}
