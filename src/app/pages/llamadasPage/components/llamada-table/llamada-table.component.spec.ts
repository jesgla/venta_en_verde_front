import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LlamadaTableComponent } from './llamada-table.component';

describe('LlamadaTableComponent', () => {
  let component: LlamadaTableComponent;
  let fixture: ComponentFixture<LlamadaTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LlamadaTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LlamadaTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
