import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'llamada-table',
  templateUrl: './llamada-table.component.html',
  styleUrls: ['./llamada-table.component.scss']
})
export class LlamadaTableComponent implements OnInit {
  @Input() innerWidth: number;
  @Input() lstLlamada: any;
  page: number = 1;
  currentPage = 1;
  itemsPerPage = 10;
  pageSize: number;

  constructor() {
 
   }

  public onPageChange(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }

  public changePagesize(num: number): void {
    this.itemsPerPage = this.pageSize + num;
  }

  ngOnInit() {
  }

}
