import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ArticulosService } from 'src/app/services/articulos/articulos.service';

@Component({
  selector: 'ventas-table',
  templateUrl: './ventas-table.component.html',
  styleUrls: ['./ventas-table.component.scss']
})
export class VentasTableComponent implements OnInit {

  @Input() innerWidth: number;
  @Input() lstVentas: any[];
  page: number = 1;
  currentPage = 1;
  itemsPerPage = 10;
  pageSize: number;
  @Input() existe: boolean;

  constructor(private articuloService: ArticulosService) {

  }

  public onPageChange(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }

  public changePagesize(num: number): void {
    this.itemsPerPage = this.pageSize + num;
  }

  ngOnInit() {
  }

}
