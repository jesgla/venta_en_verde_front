import { Component, OnInit } from '@angular/core';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ArticulosService } from 'src/app/services/articulos/articulos.service';

@Component({
  selector: 'app-llamadas',
  templateUrl: './llamadas.component.html',
  styleUrls: ['./llamadas.component.scss']
})
export class LlamadasComponent implements OnInit {
  vendedor: any;
  lstClientes: any;
  lstContestadas: any[];
  lstNoContestadas: any[];
  porcentajeContestadas: number;
  porcentajeNoContestadas: number;
  lstFiltrada: any[];
  innerWidth: any;
  currentJustify = 'justified';
  lstVentas: any[];
  tipoFiltro: string;
  existe: boolean;
  totalVentas: number;
  constructor(private vendedorService: VendedorService, private dataLocal: DataLocalService, private articuloService: ArticulosService) {
    this.lstContestadas = []
    this.lstNoContestadas = []
    this.lstVentas = [];
    this.tipoFiltro = 'diario';
    this.existe = false;
    this.totalVentas = 0
  }
  onResize(event) {

    this.innerWidth = event.target.innerWidth;




  }

  async ngOnInit() {
    this.innerWidth = window.innerWidth;
    let { vendedor, inicio, termino } = await this.dataLocal.obtenerItem('vendedor');
    this.vendedor = vendedor;
    this.lstClientes = this.vendedor.clientes;

 
    await this.obtenerFiltro(inicio, termino)
    this.porcentajeContestadas = Math.round((this.vendedor.contactados * 100) / (this.vendedor.contactados + this.vendedor.contactadosFallidos));
    this.porcentajeNoContestadas = Math.round((this.vendedor.contactadosFallidos * 100) / (this.vendedor.contactados + this.vendedor.contactadosFallidos));

  }

  async obtenerFiltro(fechaInicio, fechaTermino) {
    let arregloContestado = [];
    let arregloNoContestado = [];

    let revisar = [];
    let excel = [];

    for (let index = 0; index < this.lstClientes.length; index++) {

      const cliente = this.lstClientes[index];

      for (let indice = 0; indice < cliente.registros.length; indice++) {

        const llamada = cliente.registros[indice];
        let fechaLlamada = Number(llamada.idllamada);
        let fecha = new Date(fechaLlamada)
        let total = 0;


        if (fechaInicio < fechaLlamada && fechaLlamada <= fechaTermino && llamada.segundos > 0) {

          if (llamada.segundos > 39) {

            let ventas = this.vendedor.ventas;



            ventas.filter(venta => {
              let fechab = new Date(venta.Fecha).getTime();
              let valor = 24 * 60 * 60 * 1000 * 7;


              let fechaI = new Date(fechaLlamada).setHours(0, 0, 0, 0)
              let semana = fechaI + valor;
              let fechaT = new Date(semana).setHours(23, 59, 59, 59);
              if (venta.cliente == cliente.rut && fechaInicio < fechaLlamada && fechaLlamada <= fechaTermino && fechaI < fechab && fechab <= fechaT && this.vendedor.id === venta.codEmpleado && venta.tipoTransaccion !== "NCE") {
                total += Math.round(venta.VentaUnit * venta.cantidad)

                revisar.push({
                  rut: cliente.rut,
                  nombre: cliente.nombre,
                  fechaL: fechaLlamada,
                  fechaDoc: venta.Fecha,
                  doc: venta.numeroDocumento,
                  sku: venta.SKU,
                  venta: Math.round(venta.VentaUnit),
                  cantidad: Math.round(venta.cantidad),
                  total: Math.round(venta.VentaUnit * venta.cantidad)
                });

              }
            })


          }


        }

      }


    }

    for (let index = 0; index < revisar.length; index++) {
      let fechaI = null
      let semana = null
      let fechaT = null
      let fechab = null
      const venta = revisar[index];
      let existe = false;
      for (let i = index + 1; i < revisar.length; i++) {
        const copiaVenta = revisar[i];
        fechab = new Date(copiaVenta.fechaDoc).getTime();
        let valor = 24 * 60 * 60 * 1000 * 7;


        fechaI = new Date(venta.fechaL).setHours(0, 0, 0, 0)
        semana = fechaI + valor;
        fechaT = new Date(semana).setHours(23, 59, 59, 59);

        if (fechaI < fechab && fechab < fechaT && venta.doc == copiaVenta.doc && venta.sku == copiaVenta.sku) {
          existe = true

        }
      }
      if (!existe) {
        this.totalVentas += venta.total;

        excel.push({
          rut: venta.rut,
          nombre: venta.nombre,
          fechaL: venta.fechaL,
          fechaDoc: venta.fechaDoc,
          doc: venta.doc,
          sku: venta.sku,
          venta: venta.venta,
          cantidad: venta.cantidad,
          total: venta.total,
        })
      }
    }

    excel.sort((a, b) => {
      if (a.fechaL > b.fechaL) {
        return -1
      }
    })

    this.lstVentas = excel;
    this.existe = true

    this.lstClientes = this.vendedor.clientes.filter(cliente => cliente.registros.length > 0);
    for (let index = 0; index < this.lstClientes.length; index++) {

      const cliente = this.lstClientes[index];
      let comentarios = await this.vendedorService.obtenerEventos(cliente, this.vendedor);
      cliente.registros.filter((llamada, index) => {
        llamada.rut = cliente.rut;
        llamada.nombre = cliente.nombre;
        llamada.celular = cliente.celular;
        if (comentarios.length > 0 && comentarios[index] != undefined) {
          llamada.comentario = comentarios[index].Observaciones
        } else {
          llamada.comentario = 'Sin comentario'
        }
        if (llamada.segundos > 0) {
          this.lstContestadas.push(llamada);
        } else if (llamada.segundos == 0) {
          this.lstNoContestadas.push(llamada)
        }

      });

    }
    /*    this.lstContestadas = arregloContestado;
       this.lstNoContestadas = arregloNoContestado; */
    this.lstContestadas.sort((a, b) => {
      if (a.idllamada > b.idllamada) {
        return -1
      }
    })
    this.lstNoContestadas.sort((a, b) => {
      if (a.idllamada > b.idllamada) {
        return -1
      }
    })
  }


  exportAsXLSX(): void {
    let lstExcel = [[this.vendedor.rut, this.vendedor.nombre]];
    let noContestadas = [];
    let contestadas = [];

    for (let index = 0; index < this.lstClientes.length; index++) {
      const cliente = this.lstClientes[index];
      for (let indice = 0; indice < cliente.registros.length; indice++) {
        const llamada = cliente.registros[indice];
        let objetoArr = [cliente.rut, cliente.nombre, cliente.celular, llamada.segundos == undefined ? 0 : llamada.segundos, llamada.idllamada == undefined ? llamada.fecha : new Date(Number(llamada.idllamada)), llamada.comentario];
        if (llamada.segundos > 0) {
          contestadas.push(objetoArr)
        } else {
          noContestadas.push(objetoArr)
        }

      }

    }
    lstExcel.push([]);
    lstExcel.push(["Llamadas contestadas"]);
    lstExcel.push([]);
    lstExcel.push(['rut', 'nombre', 'celular', 'segundos', 'fecha', 'comentario']);

    contestadas.map(llamada => { lstExcel.push(llamada) })

    lstExcel.push([]);
    lstExcel.push(["Llamadas no contestadas"]);
    lstExcel.push([]);
    lstExcel.push(['rut', 'nombre', 'celular', 'segundos', 'fecha', 'comentario']);
    noContestadas.map(llamada => { lstExcel.push(llamada) })
    /*     lstExcel.sort((a, b) => {
          if (a.rut > b.rut) {
            return 1;
          }
          if (a.rut < b.rut) {
            return -1;
          }
          // a must be equal to b
          return 0;
        }); */

    this.vendedorService.exportAsExcelFile(lstExcel, 'vendedor');
  }

  exportarVentas(): void {
    let lstExcel = [[this.vendedor.rut, this.vendedor.nombre]];

    lstExcel.push([]);
    lstExcel.push(["Ventas"]);
    lstExcel.push([]);
    lstExcel.push(['Rut', 'Nombre', 'Fecha Llamada', 'Fecha Documento', 'Documento', 'SKU', 'Venta', 'Cantidad', 'Total']);

    this.lstVentas.map(venta => {
      let objetoArr = [venta.rut,
      venta.nombre,
      new Date(venta.fechaL).toLocaleString('es-CL', { timeZone: 'UTC' }),
      new Date(venta.fechaDoc).toLocaleString('es-CL', { timeZone: 'UTC' }),
      venta.doc,
      venta.sku,
      venta.venta,
      venta.cantidad,
      venta.total
      ];
      lstExcel.push(objetoArr)
    })

    this.vendedorService.exportAsExcelFile(lstExcel, 'vendedor');
  }

}
