import { Component, OnInit, HostListener, ViewChild, TemplateRef } from '@angular/core';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import * as XLSX from 'xlsx';
import { Vendedor } from 'src/app/interfaces/vendedor';
import { ModalService } from 'src/app/services/modal/modal.service';
import { NgbModal, NgbCalendar, NgbDateParserFormatter, NgbDate, NgbDatepickerI18n, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ArticulosService } from 'src/app/services/articulos/articulos.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  lstVendedores: Vendedor[];
  diaHoy: number;
  textoBuscar: string = '';
  textoBuscarArticulo: string = '';
  textoBuscarDesconectado: string = '';
  id: boolean = false;
  nombre: boolean = false;
  clientes: boolean = false;
  contactados: boolean = false;
  contactadosFallidos: boolean = false;
  minLlamadas: boolean = false;
  gerente: boolean = false;
  codTienda: boolean = false;
  tienda: boolean = false;
  public innerWidth: any;
  totalVentas: number;
  totalenProceso: number;
  totalTerminadas: number;
  totalCanceladas: number;
  totalArticulos: number;
  totalArtCompletos: number;
  totalArtIncompletos: number;
  tipoFiltro: string;
  arrayBuffer: any;
  file: File;
  lstFiltrada: any[];
  lstArticulos: any[];
  lstArtDesconectados: any[];
  page: number = 1;
  currentPage = 1;
  itemsPerPage = 20;
  pageSize: number = 1;
  pageArticulos: number = 1;
  currentPageArticulos = 1;
  itemsPerPageArticulos = 20;
  pageSizeArticulos: number = 1;
  lst: any = [];
  hoveredDate: NgbDate;
  modalCreaRef: BsModalRef;
  modalEditaRef: BsModalRef;
  modalMasivaRef: BsModalRef;
  artEditar: any;
  @ViewChild('modalCrearSku', { static: false }) modalCrearSku: TemplateRef<any>;
  @ViewChild('modalEditaSku', { static: false }) modalEditaSku: TemplateRef<any>;
  @ViewChild('modalMasivaSku', { static: false }) modalMasivaSku: TemplateRef<any>;
  fromDate: NgbDate;
  toDate: NgbDate;
  formFechas: FormGroup;
  closeResult: string;
  constructor(
    private formGroup: FormBuilder,
    private vendedorService: VendedorService,
    private articuloService: ArticulosService,
    private modalService: NgbModal,
    private dataModal: ModalService,
    private dataLocal: DataLocalService,
    private modalsService: BsModalService,
    private calendar: NgbCalendar, public formatter: NgbDateParserFormatter,
    private router: Router) {
    //this.diaHoy = new Date().getDate();
    var strfechaInicio = new Date()
    var strfechaTermino = new Date()

    strfechaTermino.setDate(new Date().getDate() + 1);
    strfechaInicio.setDate(new Date().getDate() - 14);

    this.diaHoy = new Date().getDate();
    this.tipoFiltro = 'diario';
    this.fromDate = calendar.getNext(calendar.getToday(), 'd', -14);

    this.toDate = calendar.getNext(calendar.getToday(), 'd', 1);

    this.formFechas = this.formGroup.group({
      fechaInicio: new FormControl('', Validators.required),
      fechaTermino: new FormControl('', Validators.required)
    })
  }
  onResize(event) {
    this.innerWidth = event.target.innerWidth;
  }
  goToLink(url: string) {
    window.open(url, "_blank");
  }
  async ngOnInit() {
    this.innerWidth = window.innerWidth;
    var fechaInicio = new Date()
    var fechaTermino = new Date()
    fechaTermino.setDate(fechaTermino.getDate() + 1);
    fechaInicio.setDate(fechaTermino.getDate() - 14);
    console.log('fechaInicio', fechaInicio)
    console.log('fechaTermino', fechaTermino)
    var dateinicio = (fechaInicio.getFullYear() + ("0" + (fechaInicio.getMonth() + 1)).slice(-2) + ("0" + fechaInicio.getDate()).slice(-2));
    var datetermino = (fechaTermino.getFullYear() + ("0" + (fechaTermino.getMonth() + 1)).slice(-2) + ("0" + fechaTermino.getDate()).slice(-2));
    // let { fechaInicio, fechaTermino } = this.formFechas.value;        
    // fechaInicio = fechaInicio.replace('-','').replace('-','');
    // fechaTermino = fechaTermino.replace('-','').replace('-','');
    // console.log('filtrarData fechaInicio',fechaInicio)
    // console.log('filtrarData fechaTermino',fechaTermino)
    
    //DEBO HABILITAR ESTE SERVICIO CUANDO COMPILE FINAL
    //this.lst = await this.vendedorService.obtenerDatosInforme(dateinicio, datetermino);
    this.lst = [];
    this.lstArticulos = [];
    //DEBO HABILITAR ESTE SERVICIO CUANDO COMPILE FINAL

    console.log('llamo los datos de ventas', this.lst);
    if (this.lst.length > 0) {
      this.setearDatosVentas(dateinicio, datetermino);
    } else {
      this.lstVendedores = this.lst;
      this.totalVentas = 0;
      this.totalTerminadas = 0;
      this.totalCanceladas = 0;
      this.totalenProceso = 0;
    }

    //DEBO HABILITAR ESTE SERVICIO CUANDO COMPILE FINAL
    //this.lstArticulos = await this.vendedorService.obtenerArticulosDropShipment();
    //DEBO HABILITAR ESTE SERVICIO CUANDO COMPILE FINAL

    console.log('llamo los datos de articulos', this.lstArticulos)
    if (this.lstArticulos.length > 0) {
      this.setearDatosArticulos();
    } else {
      this.totalArticulos = 0
      this.totalArtCompletos = 0;
      this.totalArtIncompletos = 0;
    }

    //this.filtrarData();
    this.traeListaDesconectados();
  }

  async setearDatosVentas(fechaInicio, fechaTermino) {
    console.log('setearDatosVentas: ', this.lst);
    this.totalVentas = 0;
    let CuentaPedidos = 0;
    let CuentaProceso = 0;
    let CuentaCanceladas = 0;
    let CuentaTerminadas = 0;
    for (let index = 0; index < this.lst.length; index++) {
      const registro = this.lst[index];
      if (registro.strCampo3 == 'Facturado' && registro.strCampo7 == 'Entregado') {
        CuentaTerminadas = CuentaTerminadas + 1;
      } else if (registro.strCampo3 == 'Cancelado') {
        CuentaCanceladas = CuentaCanceladas + 1;
      }
      else {
        CuentaProceso = CuentaProceso + 1;
      }
      CuentaPedidos = CuentaPedidos + 1;
      //console.log('CuentaPedidos: ', CuentaPedidos);
    }
    this.lstVendedores = this.lst;
    this.totalVentas = CuentaPedidos - 1;
    this.totalTerminadas = Math.round(CuentaTerminadas);
    this.totalCanceladas = Math.round(CuentaCanceladas);
    this.totalenProceso = Math.round(CuentaProceso - 1);
  }
  async setearDatosArticulos() {
    //console.log('setearDatosArticulos: ',this.lstArticulos)        
    this.totalArticulos = 0;
    let CuentaArticulos = 0;
    let CuentaArtCompletos = 0;
    let CuentaArtIncompletos = 0;
    for (let index = 0; index < this.lstArticulos.length; index++) {
      const registro = this.lstArticulos[index]
      if (registro.intCampo1 > '0' && registro.intCampo2 > '0') {
        CuentaArtCompletos = CuentaArtCompletos + 1;
      } else {
        CuentaArtIncompletos = CuentaArtIncompletos + 1;
      }
      CuentaArticulos = CuentaArticulos + 1;
      //console.log('CuentaArticulos: ',CuentaArticulos)
    }
    this.totalArticulos = CuentaArticulos - 1;
    this.totalArtCompletos = Math.round(CuentaArtCompletos);
    this.totalArtIncompletos = Math.round(CuentaArtIncompletos);
  }
  async filtrarData() {

    this.lstVendedores = undefined;
    let { fechaInicio, fechaTermino } = this.formFechas.value;
    fechaInicio = fechaInicio.replace('-', '').replace('-', '');
    fechaTermino = fechaTermino.replace('-', '').replace('-', '');
    console.log('filtrarData fechaInicio', fechaInicio)
    console.log('filtrarData fechaTermino', fechaTermino)
    this.lst = await this.vendedorService.obtenerDatosInforme(fechaInicio, fechaTermino);
    console.log('DAtos:_', this.lst);
    this.setearDatosVentas(fechaInicio, fechaTermino);
  }
  rangoFecha(fechaInicio, fechaTermino) {
    let valor = 24 * 60 * 60 * 1000 * 7;





    let fechas = {
      fechaInicio: fechaInicio, fechaTermino: fechaTermino
    }
    return fechas
  }


  incomingfile(event) {
    this.file = event.target.files[0];
  }
  onFileChange() {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      let json = XLSX.utils.sheet_to_json(worksheet, { raw: true })
      console.log('json',json);
      //this.insertarProductos(json);

    }
    fileReader.readAsArrayBuffer(this.file);
  }

  detalleLlamadas(vendedor) {
    let { fechaInicio, fechaTermino } = this.formFechas.value;

    let fechas = this.rangoFecha(fechaInicio, fechaTermino)
    let objeto = {
      vendedor: vendedor,
      inicio: fechas.fechaInicio,
      termino: fechas.fechaTermino,
    }
    this.dataLocal.guardarItem('vendedor', objeto);
    this.router.navigate(['/llamadas']);
  }

  exportAsXLSX(): void {
    let excel = [];
    excel.push(["Listado vendedores"]);
    excel.push([]);
    excel.push(['Gerente zona', 'Cod tienda', 'tienda', 'id', 'Vendedor', 'Asignados', 'Contactados', 'Intentos fallidos', 'Total Venta', 'Total duracion llamadas']);
    for (let index = 0; index < this.lstVendedores.length; index++) {
      const vendedor = this.lstVendedores[index];
      excel.push([vendedor.subgerente, vendedor.codTienda, vendedor.tienda, vendedor.id, vendedor.nombre, vendedor.clientes.length, vendedor.contactados, vendedor.contactadosFallidos, vendedor.totalVenta, `${vendedor.minLlamadas} seg.`]);

    }
    this.vendedorService.exportAsExcelFile(excel, 'listadoVendedores');
  }
  async exportarResumen() {
    let excel = [];

    let revisar = [];

    let fechas = this.rangoFecha(this.formFechas.value.fechaInicio, this.formFechas.value.fechaTermino)
    let { fechaInicio, fechaTermino } = fechas

    excel.push(["Resumen Global Vendedores"]);
    excel.push([]);
    excel.push(['Gerente zona', 'tienda', 'Cod. Vendedor', 'Vendedor', 'Rut Cliente', 'Cliente', 'Fecha llamada', 'Fecha Venta', 'Numero documento', 'SKU', 'Valor', 'Cantidad', 'Total Ventas', 'Duracion llamadas (segundos)']);

    let totalVenta = 0;
    for (let index = 0; index < this.lst.length; index++) {
      const vendedor = this.lst[index];

      totalVenta = 0;

      let clientes = vendedor.clientes;

      for (let index = 0; index < clientes.length; index++) {

        const cliente = clientes[index];

        for (let indice = 0; indice < cliente.registros.length; indice++) {

          const llamada = cliente.registros[indice];
          let fechaLlamada = Number(llamada.idllamada);
          let fecha = new Date(fechaLlamada)
          let total = 0;


          if (fechaInicio < fechaLlamada && fechaLlamada <= fechaTermino && llamada.segundos > 0) {
            if (llamada.segundos > 39) {

              let ventas = vendedor.ventas;
              ventas.filter(venta => {
                let fechab = new Date(venta.Fecha).getTime();
                let valor = 24 * 60 * 60 * 1000 * 7;


                let fechaI = new Date(fechaLlamada).setHours(0, 0, 0, 0)
                let semana = fechaI + valor;
                let fechaT = new Date(semana).setHours(23, 59, 59, 59);
                if (venta.cliente == cliente.rut && fechaInicio < fechaLlamada && fechaLlamada <= fechaTermino && fechaI < fechab && fechab <= fechaT && vendedor.id === venta.codEmpleado && venta.tipoTransaccion !== "NCE") {
                  total += Math.round(venta.VentaUnit * venta.cantidad)

                  revisar.push({
                    subgerente: vendedor.subgerente,
                    tienda: vendedor.tienda,
                    id: vendedor.id,
                    nombreV: vendedor.nombre,
                    rut: cliente.rut,
                    nombreC: cliente.nombre,
                    fechaL: fechaLlamada,
                    fechaDoc: venta.Fecha,
                    doc: venta.numeroDocumento,
                    sku: venta.SKU,
                    venta: Math.round(venta.VentaUnit),
                    cantidad: Math.round(venta.cantidad),
                    total: Math.round(venta.VentaUnit * venta.cantidad),
                    segundos: llamada.segundos
                  });

                }
              })


            }


          }

        }


      }


    }

    for (let index = 0; index < revisar.length - 1; index++) {
      const venta = revisar[index];
      let existe = false;
      for (let i = index + 1; i < revisar.length; i++) {
        const copiaVenta = revisar[i];
        let fechab = new Date(copiaVenta.fechaDoc).getTime();
        let valor = 24 * 60 * 60 * 1000 * 7;
        let fechaI = new Date(venta.fechaL).setHours(0, 0, 0, 0)
        let semana = fechaI + valor;
        let fechaT = new Date(semana).setHours(23, 59, 59, 59);

        if (fechaI < fechab && fechab < fechaT && venta.doc == copiaVenta.doc && venta.sku == copiaVenta.sku) {

          existe = true
        }
      }
      if (!existe) {
        excel.push([
          venta.subgerente,
          venta.tienda,
          venta.id,
          venta.nombreV,
          venta.rut,
          venta.nombreC,
          new Date(venta.fechaL).toLocaleString('es-CL', { timeZone: 'UTC' }),
          new Date(venta.fechaDoc).toLocaleString('es-CL', { timeZone: 'UTC' }),
          venta.doc,
          venta.sku,
          venta.venta,
          venta.cantidad,
          venta.total,
          venta.segundos
        ])
      }
    }

    excel.sort((a, b) => {
      if (a.doc < b.doc) {
        return -1
      }
    })

    await this.vendedorService.exportAsExcelFile(excel, 'ResumenGlobal');

  }

  ordenarLista(tipo, estado) {

    if (estado) {
      estado = false;

      this.lstVendedores.sort((a, b) => {
        if (a[tipo] < b[tipo]) {
          return -1;
        }


        // a must be equal to b
        return 0;
      })

    } else {
      this.lstVendedores.sort((a, b) => {
        if (a[tipo] > b[tipo]) {
          return -1;
        }
        // a must be equal to b
        return 0;
      });
    }


  }

  onDateSelection(date: NgbDate) {

    if (!this.fromDate && !this.toDate) {

      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.formFechas.controls['fechaTermino'].setValue(this.formatter.format(date))
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;

      this.formFechas.controls['fechaInicio'].setValue(this.formatter.format(date))
    }

  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {

    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  public onPageChange(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }

  public changePagesize(num: number): void {
    this.itemsPerPage = this.pageSize + num;
  }
  public onPageChangeArticulos(pageNum: number): void {
    this.pageSizeArticulos = this.itemsPerPageArticulos * (pageNum - 1);
  }

  public changePagesizeArticulos(num: number): void {
    this.itemsPerPageArticulos = this.pageSizeArticulos + num;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openModal(nombreModal,objeto) {
    switch (nombreModal) {
      case "CreaSku":
        this.modalCreaRef = this.modalsService.show(this.modalCrearSku);
        break;
      case "EditaSku":
        this.artEditar = objeto;
        this.modalEditaRef = this.modalsService.show(this.modalEditaSku);
        break;
      case "MasivaSku":
        this.modalMasivaRef = this.modalsService.show(this.modalMasivaSku);
        break;
    }

    
  }

  async traeListaDesconectados() {
    let lstDesconectados = await this.articuloService.obtenerDesconectados("");
    this.lstArtDesconectados = lstDesconectados;
    console.log("lstDesconectados",lstDesconectados);
  }

  async actualizarAfterEdit(event) {
    if (event) { 
      this.traeListaDesconectados();
    }
  }



}
