import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(arreglo: any[],
    texto: string,
    columna: string,
    columna2: string,
    columna3: string,
    columna4: string,
    columna5: string,
    columna6: string,
    columna7: string    
  ): any[] {

    if (texto.trim().length === 0) {
      return arreglo;
    }
console.log("text:",texto);
    texto = texto.toLowerCase();


    return arreglo.filter(item => {
      return item[columna].toLowerCase()
        .includes(texto) 
        ||
        item[columna2].toLowerCase()
        .includes(texto)
        ||
        item[columna3].toLowerCase()
        .includes(texto)
        ||
        item[columna4].toLowerCase()
        .includes(texto)
        ||        
        item[columna5].toLowerCase()
        .includes(texto)
        ||
        item[columna6].toLowerCase()
        .includes(texto)
        ||
        item[columna7].toLowerCase()
      .includes(texto)          
    });

  }
}
