import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltroPipe } from './filtro.pipe';
import { FormatoMonedaPipe } from './formato-moneda.pipe';



@NgModule({
  declarations: [
    FiltroPipe,
    FormatoMonedaPipe,
  ],
  imports: [
    CommonModule
  ],
  exports:[
    FiltroPipe,
    FormatoMonedaPipe]
})
export class PipesModule { }
